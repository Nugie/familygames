<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_point', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qr_code');
            $table->integer('point');
            $table->string('question');
            $table->string('user_challenge');
            $table->string('qr_code_unlock');
            $table->enum('flag_special', ['Y', 'N']);
            $table->enum('flag_free', ['Y', 'N']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_point');
    }
}
