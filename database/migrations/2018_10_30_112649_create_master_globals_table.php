<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterGlobalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_global', function (Blueprint $table) {
            $table->increments('id');
            $table->string('condition');
            $table->string('description');
            $table->enum('flag_active', ['Y', 'N']);
            $table->timestamp('dt_active');
            $table->string('user_added');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_global');
    }
}
