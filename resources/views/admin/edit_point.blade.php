@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Point</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="container">
    <div class="row">

        <div class="col-md-10">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Point</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/point') }}" class="btn btn-info btn-wkwk" role="button">List Point</a></li>
                </ol>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-body">
                    {!! Form::model($point, ['url' => '/administrator/point/save', 'role' => 'form', 'novalidate']) !!}
                    {!! Form::hidden('NO_SR', null, ['class' => 'form-control']) !!}
                    {!! Form::hidden('FLAG_SPECIAL', null, ['class' => 'form-control']) !!}
                        
                    @if($point->FLAG_SPECIAL == 'Y')
                        <div class="panel panel-default type_1" id="detail1" >
                            <div class="panel-body">
                                <div class="form-group row{{ $errors->has('question') ? ' has-error' : '' }}">
                                    {!! Form::label('QUESTION', 'Question', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('QUESTION', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('QUESTION') }}</strong>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('FLAG_FREE','Free',['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">   
                                        <label class="radio-inline"><input type="radio" name="FLAG_FREE" value="Y" {{$point->FLAG_FREE == "Y" ? 'checked' : ''}}>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="FLAG_FREE" value="N" {{$point->FLAG_FREE == "N" ? 'checked' : ''}}>No</label>
                                    </div>
                                </div>

                                 <div class="col-md-10">
                                    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate($point->QR_CODE)) !!} ">
                                    <p>Scan untuk mendapatkan QR Code point ini</p>
                                </div>

                            </div>
                        </div>
                    @elseif($point->FLAG_SPECIAL == 'N')
                        <div class="panel panel-default type_0" id="detail0" >
                            <div class="panel-body">
                                <div class="form-group row{{ $errors->has('point') ? ' has-error' : '' }}">
                                    {!! Form::label('POINT', 'Point', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('POINT', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('POINT') }}</strong>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('FLAG_FREE','Free',['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">   
                                        <label class="radio-inline"><input type="radio" name="FLAG_FREE" value="Y" {{$point->FLAG_FREE == "Y" ? 'checked' : ''}}>Yes</label>
                                        <label class="radio-inline"><input type="radio" name="FLAG_FREE" value="N" {{$point->FLAG_FREE == "N" ? 'checked' : ''}}>No</label>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate($point->QR_CODE)) !!} ">
                                    <p>Scan untuk mendapatkan QR Code point ini</p>
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="form-group row">
                            <div class="col-md-8 ">
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>
$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return decodeURI(results[1]) || 0;
    }



if($.urlParam('type') == 'success')
{
    $('.panel-default.qr_generate').show();
}
else if(empty($.urlParam()))
{
    document.getElementById('generateqr').style.display = 'none';
}

$('document').ready(function(){
    $('input[name="flag_special"]').change(function(){
      var val = $('input[name="flag_special"]:checked').val();
      if(val == 1)
      {
        document.getElementById('detail0').style.display = 'none';
        $('.panel-default.type_'+val).show();
      }
      else if(val == 0)
      {
        document.getElementById('detail1').style.display = 'none';
        $('.panel-default.type_'+val).show();
      }
    });
});


</script>
@endpush
