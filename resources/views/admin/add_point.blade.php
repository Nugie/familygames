@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Point</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="container">
    <div class="row">
        <div class="col-md-10">
                @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('gagal'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
                @endif
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Point</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/point') }}" class="btn btn-info btn-wkwk" role="button">List Point</a></li>
                </ol>

                
                @if(session('last_point'))
                    <div class="panel-body">
                        <div class="col-md-5">
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate(session('last_point')->QR_CODE)) !!} ">
                            <p>Silakan simpan qr code point yang baru saja dibuat</p>
                            <a href="{{url('/administrator/point/create')}}" class="btn btn-primary">Buat Lagi</a> 
                        </div>
                    </div>
                @else
                <div class="panel-body">
                    {!! Form::open(['url' => '/administrator/point/save', 'role' => 'form', 'novalidate']) !!}
                        <div class="form-group row">
                            {!! Form::label('flag_special','Special',['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">   
                                <label class="radio-inline"><input type="radio" name="flag_special" value="1">Yes</label>
                                <label class="radio-inline"><input type="radio" name="flag_special" value="0">No</label>
                            </div>
                        </div>

                        <div class="panel panel-default type_1" id="detail1" >
                            <div class="panel-body">
                                <div class="form-group row{{ $errors->has('question') ? ' has-error' : '' }}">
                                    {!! Form::label('question', 'Question', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('question', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('question') }}</strong>
                                        </span>
                                    </div>
                                </div>

                                <!-- <div class="form-group row{{ $errors->has('point') ? ' has-error' : '' }}">
                                    {!! Form::label('point', 'Point', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('point', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('point') }}</strong>
                                        </span>
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    {!! Form::label('flag_free','Free',['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">   
                                        <label class="radio-inline"><input type="radio" name="flag_free" value="Y">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="flag_free" value="N">No</label>
                                    </div>
                                </div>
 
                            </div>
                        </div>

                        <div class="panel panel-default type_0" id="detail0" >
                            <div class="panel-body">
                                <div class="form-group row{{ $errors->has('point') ? ' has-error' : '' }}">
                                    {!! Form::label('point', 'Point', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('point', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                        <span class="help-block">
                                            <strong>{{ $errors->first('point') }}</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {!! Form::label('flag_free','Free',['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">   
                                        <label class="radio-inline"><input type="radio" name="flag_free" value="Y">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="flag_free" value="N">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>
                        </div>
                       

                    {!! Form::close() !!}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>
window.onload = function(){
    document.getElementById('detail1').style.display = 'none';
    document.getElementById('detail0').style.display = 'none';
};

$('document').ready(function(){
    $('input[name="flag_special"]').change(function(){
      var val = $('input[name="flag_special"]:checked').val();
      if(val == 1)
      {
        document.getElementById('detail0').style.display = 'none';
        $('.panel-default.type_'+val).show();
      }
      else if(val == 0)
      {
        document.getElementById('detail1').style.display = 'none';
        $('.panel-default.type_'+val).show();
      }
    });
});


</script>
@endpush
