@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">List Player</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="col-md-11">
    <div class="panel panel-default">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Player</a>
            </li>
            <li class="breadcrumb-item active"><a href="{{url('/administrator/player/create') }}" class="btn btn-info btn-wkwk" role="button">Add Player</a></li>
            
        </ol>

        @if (session('status'))
            <div class="alert alert-warning">
                {{ session('status') }}
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered" id="player-table" width="100%" style="font-size:12px;">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Team Name</th>
                        <th>IMEI Device</th>
                        <th>Login Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#player-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/administrator/player/data') }}",
                columns: [
                    { data: 'USERNAME', name: 'USERNAME' },
                    { data: 'TEAM_NAME', name: 'TEAM_NAME'},
                    { data: 'IMEI_DEVICE', name: 'TEAM_NAME' },
                    { data: 'LOGIN_STATUS', name: 'TEAM_NAME' },
                    { data: 'action', name: 'action' , orderable: false}
                ]
            });
        });
    </script>
    @endpush