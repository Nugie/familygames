@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">List Point</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="col-md-9">
    <div class="panel panel-default">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Point</a>
            </li>
            <li class="breadcrumb-item active"><a href="{{url('/administrator/point/create') }}" class="btn btn-info btn-wkwk" role="button">Add Point</a></li>
            
        </ol>

        @if (session('status'))
            <div class="alert alert-warning">
                {{ session('status') }}
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered" id="point-table" width="100%" style="font-size:12px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Description</th>
                        <th>Point</th>
                        <th>Special</th>
                        <th>Question</th>
                        <th>Free</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="panel panel-default">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"></li>
            Global Status
        </ol>
        <div class="panel-body">
            {!! Form::model($global, ['url' => '/administrator/global/change_status', 'role' => 'form', 'novalidate']) !!}
                {!! Form::hidden('id', null, ['class' => 'form-control']) !!}
                {!! Form::label('flag_active', 'Free: ', ['class' => 'col-md-5 control-label']) !!}
                    <!-- <input id="flag_active" type="checkbox" checked data-toggle="toggle" data-on="ON" data-off="OFF" data-onstyle="success" data-offstyle="danger">
                    <input id="console-event"  type = "hidden"> -->
                            <label class="radio-inline"><input type="radio" name="flag_active" value="Y" {{$global->flag_active == "Y" ? 'checked' : ''}}>On</label>
                            <label class="radio-inline"><input type="radio" name="flag_active" value="N" {{$global->flag_active == "N" ? 'checked' : ''}}>Off</label>
                    <br><br>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-6">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
        $(function() {
            $('#point-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('/administrator/point/data') }}",
                columns: [
                    { data: 'NO_SR', name: 'NO_SR' },
                    { data: 'QR_CODE', name: 'QR_CODE' },
                    { data: 'POINT', name: 'POINT'},
                    { data: 'FLAG_SPECIAL', name: 'FLAG_SPECIAL' },
                    { data: 'QUESTION', name: 'QUESTION' },
                    { data: 'FLAG_FREE', name: 'FLAG_FREE'},
                    { data: 'action', name: 'action' , orderable: false}
                ]
            });
        });
</script>
<script>
//   $(function() {
//     $('#flag_free').bootstrapToggle();
//   })
$(function() {
    $('#flag_active').change(function() {
        if($(this).prop("checked") == true){
            $value = 'Y';
            $('#console-event').val('Toggle: ' + $value);
            // $('#console-event').val($value);
        }
        else
        {
            $value = 'N';
            $('#console-event').val('Toggle: ' + $value);
            // $('#console-event').val($value);
        }
            // $('#console-event').html('Toggle: ' + $(this).prop('checked'))

  })
});
</script>
@endpush