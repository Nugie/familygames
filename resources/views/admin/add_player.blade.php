@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Player</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="container">
    <div class="row">

        <div class="col-md-10">
            <div class="panel panel-default">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Player</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="{{url('/administrator/player') }}" class="btn btn-info btn-wkwk" role="button">List Player</a></li>
                </ol>

                @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-body">
                    {!! Form::open(['url' => '/administrator/player/save', 'role' => 'form', 'novalidate']) !!}
                        
                        <div class="form-group row{{ $errors->has('USERNAME') ? ' has-error' : '' }}">
                            {!! Form::label('USERNAME', 'Username', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('USERNAME', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('USERNAME') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row{{ $errors->has('PASSWORD') ? ' has-error' : '' }}">
                            {!! Form::label('PASSWORD', 'Password', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('PASSWORD', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('PASSWORD') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row{{ $errors->has('TEAM_NAME') ? ' has-error' : '' }}">
                            {!! Form::label('TEAM_NAME', 'Team Name', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('TEAM_NAME', null, ['class' => 'form-control', 'maxlength' => '15', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('TEAM_NAME') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row{{ $errors->has('IMEI_DEVICE') ? ' has-error' : '' }}">
                            {!! Form::label('IMEI_DEVICE', 'IMEI Device', ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-10">
                                {!! Form::text('IMEI_DEVICE', null, ['class' => 'form-control', 'required', 'autofocus']) !!}
                                <span class="help-block">
                                    <strong>{{ $errors->first('IMEI_DEVICE') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-primary">Batal</a> 
                            </div>

                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>

@endsection