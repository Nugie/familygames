@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">List Reset Data</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="col-md-11">

        @if (session('status'))
            <div class="alert alert-success text-center">
                {{ session('status') }}
            </div>
        @endif
    <div class="panel panel-default">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Reset Data</a>
            </li>
            <li class="breadcrumb-item active"><a href="{{url('/home') }}" class="btn btn-info" role="button">Dashboard</a></li>
            
        </ol>
        <div class="table-responsive">
            <table class="table table-bordered" id="trans-table" width="100%" style="font-size:12px;">
                <thead>
                    <tr>
                        <th>Team</th>
                        <tH>QR Code</tH>
                        <th>Point</th>
                        <th>Flag Reset</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $(function() {
        $('#trans-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[1, "desc"]],
            ajax: "{{ url('/administrator/transaction/reset_data') }}",
            columns: [
                { data: 'USERNAME', name: 'USERNAME' },
                { data: 'QR_CODE', name : 'QR_CODE'},
                { data: 'POINT', name : 'POINT'},
                { data: 'FLAG_RESET', name : 'FLAG_RESET'},
                { data: 'action', name : 'action'}
            ]
        });
    });
</script>
@endpush


