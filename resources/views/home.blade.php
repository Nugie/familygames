@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Leaderboard</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="col-md-11">

        @if (session('status'))
            <div class="alert alert-success text-center">
                {{ session('status') }}
            </div>
        @endif
    <div class="panel panel-default">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Leaderboard</a>
            </li>
            <li class="breadcrumb-item active"><a href="{{url('/administrator/transaction/reset') }}" class="btn btn-info btn-wkwk" role="button">Reset</a></li>
            
        </ol>
        <div class="table-responsive">
            <table class="table table-bordered" id="trans-table" width="100%" style="font-size:12px;">
                <thead>
                    <tr>
                        <th>Team</th>
                        <th>Point</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $(function() {
        $('#trans-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[1, "desc"]],
            ajax: "{{ url('/administrator/transaction/data') }}",
            columns: [
                { data: 'TEAM_NAME', name: 'TEAM_NAME' },
                { data: 'COUNT', name : 'COUNT'}
            ]
        });
    });
</script>
@endpush


