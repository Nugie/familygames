<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li>
                <a href="{{url('/home')}}"><i class="fa fa-dashboard fa-fw"></i> Home</a>
            </li>
            <li>
                <a href="{{url('/administrator/player')}}"><i class="fa fa-users fa-fw"></i> Player Registration</a>
            </li>

            <li>
                <a href="{{url('/administrator/point')}}"><i class="fa fa-gears fa-fw"></i> Master Point</a>
            </li>
            
            <li>
                <a href="{{url('/administrator/transaction/reset')}}"><i class="fa fa-undo fa-fw"></i> Reset Data</a>
            </li>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>

<!-- /.navbar-static-side -->