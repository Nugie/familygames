<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\MasterGlobal;
use App\Model\Player;
use App\Model\Point;
use App\Model\TransactionScan;
use DB;
use DataTables;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;


class AdminController extends Controller
{
    /*=====Player=====*/
    public function playerIndex()
    {
        return view('admin.list_player');
    }

    public function playerCreate()
    {
        return view('admin.add_player');
    }

    public function playerDataTable()
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewallplayer'; 
            
        $model = (object) [];
        $newModel = json_encode($model);
        
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        
        $data = json_decode($balikan->getBody()->getContents())->results;
        
        return DataTables::of($data)
            ->addColumn('action', function($data){
                    return '<a href="'.url('/administrator/player').'/'.$data->USERNAME.'/edit" class="btn btn-primary" style="margin-right:1px"><i class="fa fa-pencil"></i></a><a href="'.url('/administrator/player').'/'.$data->USERNAME.'/delete" data-id="'.$data->USERNAME.'" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></a>';
                })
            ->make(true);
    }

    public function playerEditDelete($username, $type)
    {
        if($type == 'edit')
        {
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewplayer';
            
            // WIRA
            $model = (object) [
                "USERNAME" => $username
            ];
            $editPlayer = ["newPlayer"=>$model];
            $newModel = json_encode($editPlayer);
            
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            $player = json_decode($balikan->getBody()->getContents())->OUT_DATA[0];
            
            return view('admin.edit_player', compact('player'));
        }
        if($type == 'delete')
        {
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/deleteplayer';
            
            // WIRA
            $model = (object) [
                "USERNAME" => $username
            ];
            $deletePlayer = ["newPlayer"=>$model];
            $newModel = json_encode($deletePlayer);
            
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            
            return redirect('/administrator/player')->with('status', 'Data player berhasil dihapus');
        }
    }

    public function playerStore(Request $request)
    {
        //edit
        if(!empty($request->USER_ADDED))
        {
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/updateplayer'; 
            
            // WIRA
            $model = (object) [
                "USERNAME" => $request->USERNAME,
                "PASSWORD"=> $request->PASSWORD,
                "TEAM_NAME"=> $request->TEAM_NAME,
                "IMEI_DEVICE"=> $request->IMEI_DEVICE,
                "LOGIN_STATUS"=>"0",
                "DT_ADDED"=>NULL,
                "USER_ADDED"=> "IT",
                "FLAG_ACTIVE"=>"Y"
            ];
            $newPlayer = ["newPlayer"=>$model];
            $newModel = json_encode($newPlayer);
            // dd($newModel);
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            // $balikan = $client->request('POST',$url,array());
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            
            return redirect('/administrator/player')->with('status', 'Data player berhasil diupdate');
        }
        //add
        else
        {

            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/checkusername';
            $model = (object) [
                "USERNAME" => $request->USERNAME
            ];
            $updatePlayer = ["newPlayer"=>$model];
            $newModel = json_encode($updatePlayer);
            
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            $cek = json_decode($balikan->getBody()->getContents())->OUT_DATA; 
            
            if($cek == '0')
            {
                $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/newplayer'; 
            
                // WIRA
                $model = (object) [
                    "USERNAME" => $request->USERNAME,
                    "PASSWORD"=> $request->PASSWORD,
                    "TEAM_NAME"=> $request->TEAM_NAME,
                    "IMEI_DEVICE"=> $request->IMEI_DEVICE,
                    "LOGIN_STATUS"=>"0",
                    "DT_ADDED"=>NULL,
                    "USER_ADDED"=> "IT",
                    "FLAG_ACTIVE"=>"Y"
                ];
                $newPlayer = ["newPlayer"=>$model];
                $newModel = json_encode($newPlayer);
                // dd($newModel);
                $client = new Client([
                    "headers" => [
                        "token" => "1234567890",
                        "Content-Type" => "application/json",
                    ],
                ]);
                // $balikan = $client->request('POST',$url,array());
                $balikan = $client->post($url,[
                    "body" => $newModel
                ]);

                
                return redirect('/administrator/player')->with('status', 'Data player berhasil ditambahkan');
            }
            else
            {
                return redirect('/administrator/player/create')->with('gagal', 'USERNAME anda sudah ada');
            }
           
        }
    }
    /*=====End Player=====*/

    /*=====Point=====*/
    public function pointIndex()
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/getcurrentstglobal'; 
            
        
        $model = (object) [];
        $newModel = json_encode($model);
        
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        
        $global = json_decode($balikan->getBody()->getContents())->OUT_DATA[0];
        
        return view('admin.list_point', compact('global'));
    }

    public function pointCreate()
    {
        $point = Point::where('flag_special', 'N')->get();
        return view('admin.add_point', compact('point'));
    }

    public function pointDataTable()
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewallpoint'; 
            
        
        $model = (object) [];
        $newModel = json_encode($model);
        
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        
        $data = json_decode($balikan->getBody()->getContents())->results;
        
        return DataTables::of($data)
            ->addColumn('action', function($data){
                    return '<a href="'.url('/administrator/point').'/'.$data->NO_SR.'/edit" class="btn btn-primary" style="margin-right:1px"><i class="fa fa-pencil"></i></a><a href="'.url('/administrator/point').'/'.$data->NO_SR.'/delete" data-id="'.$data->NO_SR.'" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></a>';
                })
            ->make(true);
    }

    public function pointEditDelete($id, $type)
    {
        if($type == 'edit')
        {
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewpoint';
            
            // WIRA
            $model = (object) [
                "NO_SR" => $id
            ];
            $editPoint = ["insertPoint"=>$model];
            $newModel = json_encode($editPoint);
            
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            $point = json_decode($balikan->getBody()->getContents())->results[0];
            return view('admin.edit_point', compact('point'));
        }
        if($type == 'delete')
        {
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/deletepoint';
            
            // WIRA
            $model = (object) [
                "NO_SR" => $id
            ];
            $editPoint = ["insertPoint"=>$model];
            $newModel = json_encode($editPoint);
            
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);

            return redirect('/administrator/point')->with('status', 'Data point berhasil dihapus');
        }
    }

    public function pointStore(Request $request)
    {
        /* GET ID */
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewallpoint'; 
        $model = (object) [];
        $newModel = json_encode($model);
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        $id = strval(count(json_decode($balikan->getBody()->getContents())->results)+1);

        //edit
        if(!empty($request->NO_SR))
        {
            /* EDIT */
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/updatepoint';
            $model = (object) [
                "NO_SR" => $request->NO_SR,
                "QR_CODE" => $request->QR_CODE,
                "POINT" => $request->POINT,
                "QUESTION" => $request->QUESTION,
                "USER_CHALENGE" => "",
                "QR_CODE_UNLOCK" => "",
                "FLAG_SPECIAL" => $request->FLAG_SPECIAL,
                "FLAG_FREE" => $request->FLAG_FREE, 
            ];
            $updatePoint = ["insertPoint"=>$model];
            $newModel = json_encode($updatePoint);
            // dd($newModel);
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            // $balikan = $client->request('POST',$url,array());
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);

            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewpoint'; 
            $model = (object) [
                "NO_SR" => $request->NO_SR
            ];
            $editPoint = ["insertPoint"=>$model];
            $newModel = json_encode($editPoint);
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);

            $point = json_decode($balikan->getBody()->getContents());
            // dd($point);
            return redirect(url('/administrator/point/'.$request->NO_SR.'/edit'))->with(compact('point'))->with('status', 'Data Point berhasil diupdate');
        }
        //create
        else
        {   
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/insertpoint'; 
                       
            if($request->flag_special == '0')
            {
                $model = (object) [
                    "NO_SR" => $id,
                    "QR_CODE" => "ACC".$id,
                    "POINT" => $request->point,
                    "QUESTION" => "",
                    "USER_CHALENGE" => "",
                    "QR_CODE_UNLOCK" => "",
                    "FLAG_SPECIAL" => "N",
                    "FLAG_FREE" => $request->flag_free, 
                ];
            }
            else if($request->flag_special == '1')
            {
                $model = (object) [
                    "NO_SR" => $id,
                    "QR_CODE" => "ACC".$id."S",
                    "POINT" => $request->point,
                    "QUESTION" => $request->question,
                    "USER_CHALENGE" => "",
                    "QR_CODE_UNLOCK" => "",
                    "FLAG_SPECIAL" => "Y",
                    "FLAG_FREE" => $request->flag_free, 
                ];
            }
            $newPoint = ["insertPoint"=>$model];
            $newModel = json_encode($newPoint);
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            /* END CREATE */
            /* GET LAST DATA */
            $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/viewpoint'; 
            $model = (object) [
                "NO_SR" => $id
            ];
            $editPoint = ["insertPoint"=>$model];
            $newModel = json_encode($editPoint);
            $client = new Client([
                "headers" => [
                    "token" => "1234567890",
                    "Content-Type" => "application/json",
                ],
            ]);
            
            $balikan = $client->post($url,[
                "body" => $newModel
            ]);
            
            $last_point = json_decode($balikan->getBody()->getContents())->results[0];
            return redirect(url('/administrator/point/create?type=success'))->with(compact('last_point'))->with('status', 'Data Point berhasil ditambahkan');
        }
    }

    /*=====End Point=====*/
    /*=====Transaction=====*/
    public function transactionIndex()
    {
        return view('home');
    }

    public function transDataTable()
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/dashboarddata'; 
        $model = (object) [];
        $newModel = json_encode($model);
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        $data = json_decode($balikan->getBody()->getContents())->OUT_DATA;
        
        return DataTables::of($data)
            ->make(true);
    }

    public function transReset()
    {
        return view('admin.resetdata');
    }

    public function transResetData()
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/listdataplayer'; 
            
        $model = (object) [];
        $newModel = json_encode($model);
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        $data = json_decode($balikan->getBody()->getContents())->OUT_DATA;
        
        return DataTables::of($data)
            ->addColumn('action', function($data){
                return '<a href="'.url('/administrator/transaction/reset_data').'/'.$data->USERNAME.'" data-id="'.$data->USERNAME.'" class="btn btn-warning"><i class="fa fa-undo"> Reset</i></a>';
            })
            ->make(true);
    }
    public function transResetDataPro($username)
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/updateflagreset'; 
            
        $model = (object) [
            "USERNAME" => $username,
        ];
        $dataUser = ["dataUser"=>$model];
        $newModel = json_encode($dataUser);
        
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        return redirect('/administrator/transaction/reset')->with('status', 'Data berhasil direset');
    }
    /*=====End Transaction=====*/
    /*=====Global=====*/
    public function changeStatusGlobal(Request $request)
    {
        $url = 'https://sofiadev.acc.co.id/rest/com/hr/in/httprest/scanGame/updatemstglobal'; 
            
        $model = (object) [
            "CONDITION" => "ONOFF_FREE",
            "FLAG_ACTIVE"=> $request->flag_active,
        ];
        $mstGlobal = ["mstGlobal"=>$model];
        $newModel = json_encode($mstGlobal);
        
        $client = new Client([
            "headers" => [
                "token" => "1234567890",
                "Content-Type" => "application/json",
            ],
        ]);
        
        $balikan = $client->post($url,[
            "body" => $newModel
        ]);
        
        return redirect('/administrator/point');
    }
    /*=====End Global=====*/
  
}
