<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends Model
{
    //
    use SoftDeletes;
    protected $table = 'mst_player';
    protected $fillable = [
        'username', 'password', 'team_name', 'imei_device', 'login_status', 'user_added', 'flag_active'
    ];
}
