<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Point extends Model
{
    //
    use SoftDeletes;
    protected $table = 'mst_point';
    protected $fillable = [
        'question', 'qr_code', 'point', 'user_challenge', 'qr_code_unlock', 'flag_special', 'flag_free'
    ];
}
