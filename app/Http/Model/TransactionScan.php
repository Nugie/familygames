<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionScan extends Model
{
    //
    protected $table = 'trn_scan_code';
    protected $fillable = [
        'no_tran', 'username', 'qr_code', 'point',
    ];
}
