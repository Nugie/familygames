<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterGlobal extends Model
{
    //
    protected $table = 'mst_global';
    protected $fillable = [
        'condition', 'description', 'flag_active', 'dt_active', 'user_added'
    ];
}
