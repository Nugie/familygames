<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/administrator/player', 'AdminController@playerIndex');
Route::get('/administrator/player/create', 'AdminController@playerCreate');
Route::post('/administrator/player/save', 'AdminController@playerStore');
Route::get('/administrator/player/data', 'AdminController@playerDataTable');
Route::get('/administrator/player/{username}/{type}', 'AdminController@playerEditDelete');

Route::get('/administrator/point', 'AdminController@pointIndex');
Route::get('/administrator/point/create', 'AdminController@pointCreate');
Route::post('/administrator/point/save', 'AdminController@pointStore');
Route::get('/administrator/point/data', 'AdminController@pointDataTable');
Route::get('/administrator/point/{id}/{type}', 'AdminController@pointEditDelete');

Route::get('/administrator/transaction', 'AdminController@index');
Route::get('/administrator/transaction/reset', 'AdminController@transReset');
Route::get('/administrator/transaction/reset_data', 'AdminController@transResetData');
Route::get('/administrator/transaction/reset_data/{username}', 'AdminController@transResetDataPro');
Route::get('/administrator/transaction/data', 'AdminController@transDataTable');
Route::post('/administrator/transaction/save/{player}/{qrcode}', 'AdminController@transAdd');

Route::post('/administrator/global/change_status', 'AdminController@changeStatusGlobal');

 
// Route::get('qr-code', function () {
//     return QrCode::size(500)->generate('Welcome to kerneldev.com!');
// });

Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

